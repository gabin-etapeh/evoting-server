package com.example.evotingserver.controller;

import com.example.evotingserver.models.Election;
import com.example.evotingserver.models.dto.ElectionDto;
import com.example.evotingserver.payload.response.ResponseMessage;
import com.example.evotingserver.service.ElectionService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/elections")
public class ElectionController {

  @Autowired
  private ElectionService electionService;

  @PostMapping("/add")
  public ResponseEntity<ResponseMessage> createElection(
      @RequestBody ElectionDto electionDto) {
    ResponseMessage responseMessage = ResponseMessage.builder()
        .data(electionService.createElection(electionDto))
        .message("Election created successfully!")
        .code("E00002")
        .status("CREATED")
        .build();
    return new ResponseEntity<>(responseMessage, HttpStatus.CREATED);
  }
  @GetMapping("/get")
  public ResponseEntity<List<Election>> getElection() {
    return new ResponseEntity<>(electionService.getElection(), HttpStatus.OK);
  }
  @GetMapping("/test")
  public ResponseEntity<String> testElection() {
    return new ResponseEntity<>("Hello" , HttpStatus.OK);
  }
}
