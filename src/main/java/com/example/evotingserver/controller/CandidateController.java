package com.example.evotingserver.controller;

import com.example.evotingserver.models.Candidate;
import com.example.evotingserver.service.CandidateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/candidats")
public class CandidateController {

  @Autowired
  private CandidateService candidateService;

  @PostMapping("/ajouter")
  public Candidate ajouterCandidate(@RequestBody Candidate candidate) {
    return candidateService.ajouterCandidate(candidate);
  }
}
