package com.example.evotingserver.controller;

import com.example.evotingserver.models.dto.ElectionTypeDto;
import com.example.evotingserver.payload.response.ResponseMessage;
import com.example.evotingserver.service.ElectionTypeService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/elections-type")
public class ElectionTypeController {

  private final ElectionTypeService electionTypeService;

  @PostMapping("/add")
  public ResponseEntity<ResponseMessage> createElectionType(
      @RequestBody ElectionTypeDto electionTypeDto){
    ResponseMessage responseMessage = ResponseMessage.builder()
        .data(electionTypeService.createElectionType(electionTypeDto))
        .message("Election type created successfully!")
        .code("E00001")
        .status("CREATED")
        .build();
    return new ResponseEntity<>(responseMessage, HttpStatus.CREATED);
  }
}
