package com.example.evotingserver.controller;

import com.example.evotingserver.models.EnumRole;
import com.example.evotingserver.models.Role;
import com.example.evotingserver.models.User;
import com.example.evotingserver.payload.request.LoginRequest;
import com.example.evotingserver.payload.request.SignupRequest;
import com.example.evotingserver.payload.response.ResponseMessage;
import com.example.evotingserver.payload.response.UserInfoResponse;
import com.example.evotingserver.repository.RolesRepository;
import com.example.evotingserver.repository.UsersRepository;
import com.example.evotingserver.security.jwt.JwtUtils;
import com.example.evotingserver.security.services.UserDetailsImpl;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseCookie;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashSet;
import java.util.List;
import java.util.Set;


@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/auth")
public class AuthController {

    private final AuthenticationManager authenticationManager;
    private final UsersRepository userRepository;
    private final RolesRepository roleRepository;
    private final JwtUtils jwtUtils;
    private final PasswordEncoder encoder;

    private static final String ROLE_NOT_FOUND_ERROR_MESSAGE = "Error: Role is not found.";

    @PostMapping("/login")
    public ResponseEntity<UserInfoResponse> authenticateUser(@RequestBody LoginRequest loginRequest) {

        Authentication authentication = authenticationManager
            .authenticate(new UsernamePasswordAuthenticationToken(
                loginRequest.username(),
                loginRequest.password())
            );
        SecurityContextHolder.getContext().setAuthentication(authentication);
        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        ResponseCookie jwtCookie = jwtUtils.generateJwtCookie(userDetails);

        List<String> roles = userDetails.getAuthorities().stream()
            .map(GrantedAuthority::getAuthority)
            .toList();

        return ResponseEntity.ok().header(HttpHeaders.SET_COOKIE, jwtCookie.toString())
            .body(new UserInfoResponse(userDetails.id(),
                userDetails.username(),
                userDetails.email(),
                roles));
    }

    @PostMapping("/signup")
    public ResponseEntity<ResponseMessage> registerUser(@RequestBody SignupRequest signUpRequest) {
        boolean existsByUsername = userRepository.existsByUsername(signUpRequest.username());
        if (existsByUsername) {
            return ResponseEntity.badRequest()
                .body(ResponseMessage.builder()
                    .message("Error: Username is already taken!")
                    .build());
        }
        boolean existsByEmail = userRepository.existsByEmail(signUpRequest.email());
        if (existsByEmail) {
            return ResponseEntity.badRequest()
                .body(ResponseMessage.builder()
                    .message("Error: Email is already in use!")
                    .build());
        }

        // Create new user's account
        User user = User.builder()
            .email(signUpRequest.email())
            .username(signUpRequest.username())
            .password(encoder.encode(signUpRequest.password()))
            .build();
        Set<String> stringRoles = signUpRequest.roles();
        Set<Role> roles = new HashSet<>();

        if (stringRoles == null) {
            Role userRole = roleRepository.findByRoleName(EnumRole.USER)
                .orElseThrow(() -> new RuntimeException(ROLE_NOT_FOUND_ERROR_MESSAGE));
            roles.add(userRole);
        } else {
            stringRoles.forEach(role -> {
                if (role.equalsIgnoreCase("admin")) {
                    Role adminRole = roleRepository.findByRoleName(EnumRole.ADMIN)
                        .orElseThrow(() -> new RuntimeException(ROLE_NOT_FOUND_ERROR_MESSAGE));
                    roles.add(adminRole);
                }
                Role userRole = roleRepository.findByRoleName(EnumRole.USER)
                    .orElseThrow(() -> new RuntimeException(ROLE_NOT_FOUND_ERROR_MESSAGE));
                roles.add(userRole);
            });
        }

        user.setRoles(roles);
        userRepository.save(user);

        return ResponseEntity.ok(ResponseMessage.builder()
            .message("User registered successfully!")
            .build());
    }

    @PostMapping("/logout")
    public ResponseEntity<ResponseMessage> logoutUser() {
        ResponseCookie cookie = jwtUtils.getCleanJwtCookie();
        return ResponseEntity.ok()
            .header(HttpHeaders.SET_COOKIE, cookie.toString())
            .body(ResponseMessage.builder()
                .message("You've been signed out!")
                .build());
    }
}