package com.example.evotingserver.payload.request;

import lombok.Builder;

@Builder
public record LoginRequest(String username,
                           String password) {}
