package com.example.evotingserver.payload.request;

import java.util.Set;
import lombok.Builder;

@Builder
public record SignupRequest(String username,
                            String email,
                            String password,
                            Set<String> roles) {}
