package com.example.evotingserver.payload.response;

import java.util.List;
import lombok.Builder;

@Builder
public record UserInfoResponse(Long id,
                               String username,
                               String email,
                               List<String> roles) {}
