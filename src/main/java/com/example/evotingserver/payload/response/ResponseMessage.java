package com.example.evotingserver.payload.response;

import lombok.Builder;

@Builder
public record ResponseMessage(Object data,
                              String message,
                              String code,
                              String status) {}

