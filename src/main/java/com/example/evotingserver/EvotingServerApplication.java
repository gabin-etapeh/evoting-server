package com.example.evotingserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EvotingServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(EvotingServerApplication.class, args);
	}

}
