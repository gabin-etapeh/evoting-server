package com.example.evotingserver.repository;

import com.example.evotingserver.models.ElectionType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ElectionTypeRepository extends JpaRepository<ElectionType, Long> {

}
