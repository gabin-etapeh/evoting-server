package com.example.evotingserver.repository;

import com.example.evotingserver.models.EnumRole;
import com.example.evotingserver.models.Role;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RolesRepository extends JpaRepository<Role, Long> {
    Optional<Role> findByRoleName(EnumRole roleName);
}
