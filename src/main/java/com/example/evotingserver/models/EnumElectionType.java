package com.example.evotingserver.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@ToString
@Getter
@AllArgsConstructor
public enum EnumElectionType {

    PRESIDENT("PRESIDENT"),
    DELEGUE_NIVEAU ("DELEGUE NIVEAU"),
    DELEGUE_CLASSE ("DELEGUE CLASSE");

    private String value;
}

