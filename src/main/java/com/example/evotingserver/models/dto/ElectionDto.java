package com.example.evotingserver.models.dto;

import lombok.Builder;

@Builder
public record ElectionDto(Long id,
                          String openingDate,
                          String closingDate,
                          String description,
                          String qrCode,
                          Long typeId) {
}
