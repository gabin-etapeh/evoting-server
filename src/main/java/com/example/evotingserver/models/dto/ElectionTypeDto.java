package com.example.evotingserver.models.dto;

import lombok.Builder;

@Builder
public record ElectionTypeDto(Long id,
                              String name,
                              String description,
                              String type) {}
