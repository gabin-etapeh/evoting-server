package com.example.evotingserver.models;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OneToOne;
import java.time.LocalDateTime;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class Election {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private LocalDateTime openingDate;
    private LocalDateTime closingDate;
    private String description;
    private String qrCode;

    @OneToMany (mappedBy = "election", targetEntity = Candidate.class)
    private List<Candidate> candidates;

    @OneToOne(targetEntity = ElectionType.class)
    private ElectionType type;
}
