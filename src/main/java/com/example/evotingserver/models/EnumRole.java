package com.example.evotingserver.models;

import lombok.Getter;
import lombok.ToString;

@ToString
@Getter
public enum EnumRole {
    ADMIN,
    USER
}