package com.example.evotingserver.service;

import com.example.evotingserver.models.Candidate;
import com.example.evotingserver.repository.CandidateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CandidateService {

  @Autowired
  private CandidateRepository candidateRepository;

  public Candidate ajouterCandidate(Candidate candidate) {
    return candidateRepository.save(candidate);
  }

}
