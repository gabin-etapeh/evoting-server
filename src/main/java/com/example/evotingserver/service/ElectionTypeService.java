package com.example.evotingserver.service;

import com.example.evotingserver.models.ElectionType;
import com.example.evotingserver.models.EnumElectionType;
import com.example.evotingserver.models.dto.ElectionTypeDto;
import com.example.evotingserver.repository.ElectionTypeRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class ElectionTypeService {

  private final ElectionTypeRepository repository;

  public ElectionTypeDto createElectionType(ElectionTypeDto electionTypeDto) {
    ElectionType electionType = ElectionType.builder()
        .name(electionTypeDto.name())
        .description(electionTypeDto.description())
        .type(EnumElectionType.valueOf(electionTypeDto.type()))
        .build();

    ElectionType createdType = repository.save(electionType);

    return ElectionTypeDto.builder()
        .id(createdType.getId())
        .name(createdType.getName())
        .description(createdType.getDescription())
        .type(createdType.getType().getValue())
        .build();

  }
}
