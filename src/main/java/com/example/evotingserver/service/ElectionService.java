package com.example.evotingserver.service;

import com.example.evotingserver.models.Election;
import com.example.evotingserver.models.ElectionType;
import com.example.evotingserver.models.dto.ElectionDto;
import com.example.evotingserver.repository.ElectionRepository;
import com.example.evotingserver.repository.ElectionTypeRepository;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class ElectionService {

  private final ElectionRepository electionRepository;
  private final ElectionTypeRepository electionTypeRepository;
  private final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");

  public ElectionDto createElection(ElectionDto electionDto) {
    // Chercher le type d'election à créer
    ElectionType electionType = electionTypeRepository
        .findById(electionDto.typeId())
        .orElseThrow(() -> new IllegalArgumentException("Election type not found."));

    LocalDateTime openingDate = LocalDateTime.parse(electionDto.openingDate(), formatter);
    LocalDateTime closingDate = LocalDateTime.parse(electionDto.closingDate(), formatter);

    Election election=  Election.builder()
        .type(electionType)
        .openingDate(openingDate)
        .closingDate(closingDate)
        .description(electionDto.description())
        .qrCode(electionDto.qrCode())
        .build();
    Election createdElection = electionRepository.save(election);

    return ElectionDto.builder()
        .id(createdElection.getId())
        .closingDate(createdElection.getClosingDate().format(formatter))
        .openingDate(createdElection.getOpeningDate().format(formatter))
        .description(createdElection.getDescription())
        .qrCode(createdElection.getQrCode())
        .typeId(createdElection.getType().getId())
        .build();
  }


  public Election  getElection(Long id) {
    return electionRepository.findById(id)
        .orElse(null);
  }


  public List<Election> getElection() {
    return electionRepository.findAll();
  }

}
